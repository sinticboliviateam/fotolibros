import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fotolibroapp/componets/cardLogin.dart';
/* import 'package:flutter_screenutil/'; */

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
      
          Column(crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[
            Padding(
              child: Image.asset("assets/img/foto.jpg"),
              padding: EdgeInsets.only(top: 20.0),
            ),
            Expanded(
              
              child: new Container(
           
           child: CardLogin(),
            )
             
            ),
            /* Image.asset("assets/img/foto.jpg") */
          ]),

          
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 30.0, right: 28.0, top: 30.0),
              child: Container(
                  width: 100.0,
                //  color: Colors.red,
                  height: 40.0,
                  child: Column(
               
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                       
                          Icon(
                          
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 30.0,
                            semanticLabel:
                                'Text to announce in accessibility modes',
                          ),
                     
                     Column(
                         
                           
                          children:<Widget>[
                          Center(

                            child: Padding(
                              padding: EdgeInsets.only(left: 60.0,right: 60.0),
                              child: Text(
                                "Iniciar sesión",
                                style: TextStyle(color: Colors.white,fontSize: 20.0),
                                 )
                             ),
                          ),
                          ]  
                        ),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                            size: 30.0,
                            semanticLabel:
                                'Text to announce in accessibility modes',
                          ),
                          
                    
                     
                      
                    ],
                  )
                ],
              )
                
                ),
             
            ),
          ),
        ],
        //   child: new Center(child: new Text("Page login"),),
      ),
    );
  }
}
