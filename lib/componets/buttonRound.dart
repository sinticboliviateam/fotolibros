import 'package:flutter/material.dart';

class ButtonRound extends StatelessWidget {
  const ButtonRound({Key key , @required this.buttonColor:Colors.white24, @required this.iconButton }) : super(key: key);

final Icon iconButton;
final Color buttonColor;


  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5.0),
        child: new SizedBox.fromSize(
          size: Size(60, 60), // button width and height
          child: ClipOval(
            child: Material(
              color: buttonColor  , // button color
              child: InkWell(
                splashColor: Colors.cyanAccent, // splash color
                onTap: () {}, // button pressed
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    /* Icon(iconButton,size: 40,color: colorButton,),  */// icon
                    iconButton
                   // text
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}