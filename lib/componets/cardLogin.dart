import 'package:flutter/material.dart';
import 'package:fotolibroapp/componets/buttonRaizen.dart';
import 'package:fotolibroapp/componets/buttonRound.dart';
import 'package:fotolibroapp/componets/inputTextField.dart';
import 'package:fotolibroapp/mi_custum_icons_icons.dart';

class CardLogin extends StatefulWidget {
  CardLogin({Key key}) : super(key: key);

  @override
  CardLoginState createState() => CardLoginState();
}

class CardLoginState extends State<CardLogin> {
  @override
  Widget build(BuildContext context) {
    return Container(
        // decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
        height: 420.0,
        
        decoration: new BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 20.0, // has the effect of softening the shadow
                spreadRadius: 5.0, // has the effect of extending the shadow
                offset: Offset(
                  10.0, // horizontal, move right 10
                  10.0, // vertical, move down 10
                ),
              )
            ],
            borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(40.0),
              topRight: const Radius.circular(40.0),
            )),
        child: new Padding(
          padding: EdgeInsets.all(40.0),
          child: Container(
            
              // color: Colors.red,
              child: Padding(
            padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 40.0),
            child: Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15.0, right: 15.0),
                      child: Column(
                        children: <Widget>[
                          InputTextField(
                            placeholder: "Nombre de usuario",
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                              child: InputTextField(placeholder: "Contraseña")),
                        ],
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 7.0, bottom: 7.0),
                        child: Center(
                          child: Text(
                            "Ó inicia con",
                            style: new TextStyle(
                                color: Colors.greenAccent, fontSize: 17.0),
                          ),
                        )),
                    Center(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ButtonRound(
                          iconButton: new Icon(Icons.email,size: 35,color: Colors.white),
                          buttonColor:Colors.cyan ,
                        ),
                        ButtonRound(
                          iconButton: new Icon(MiCustumIcons.facebook,size: 35,color: Colors.white),
                          buttonColor:Colors.blue ,

                        ),
                        ButtonRound(
                          iconButton: new Icon(MiCustumIcons.google,size: 35,color: Colors.white),
                          buttonColor:Colors.redAccent ,

                        )
                      ],
                    )),
                    Container(
                      color: Colors.pink,
                      height: 4.0,
                      margin: EdgeInsets.only(right: 70.0,left: 70.0, top:10.0, bottom: 10.0),


                    ),
                    Padding(
                        padding: EdgeInsets.only(
                          top: 7.0, right: 10.0, left:10.0
                        ),
                        child: ButtonRaizen(
                          text: "Crear cuenta",
                          onPressed: () {
                            print("este se hizo click");
                             Navigator.pushNamed(context, '/createAcount');
                          },
                        ))
                  ],
                )),
          )),
        ));
  }
}
