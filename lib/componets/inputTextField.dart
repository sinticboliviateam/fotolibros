import 'package:flutter/material.dart';
class InputTextField extends StatelessWidget {
  const InputTextField({Key key,  this.placeholder}) : super(key: key);
   final String placeholder;
  @override
  Widget build(BuildContext context) {
    return  Container(
                        //padding: EdgeInsets.only(left: 50.0),
                           /*  height: 60.0,
                           width:200.0, */
                        decoration: BoxDecoration(
                         //  color: Colors.blue,
                          
                          //shape: BoxShape.rectangle,
                          //  border: Border.all(width:5)
                         /*  border: Border(
                            right: BorderSide( //                   <--- left side
                                color: Colors.pink,
                                width: 5.0,
                              ),
                              top: BorderSide( //                    <--- top side
                                color: Colors.pink,
                                width: 5.0,
                                
                              ),

                               bottom: BorderSide( //                    <--- top side
                                color: Colors.pink,
                                width: 3.0,
                              ),
                            
                          ), */
                          //   borderRadius: BorderRadius.horizontal(right:Radius.circular(20.0)  )
                          //borderRadius: BorderRadius.only( topRight: const Radius.circular(20.0), bottomRight:const Radius.circular(20.0), )
                        ),
                        child: TextField(
                          decoration: InputDecoration(
                            hintText:placeholder,
                            fillColor: Colors.white,
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(30.0),
                                    bottomRight: Radius.circular(30.0)),
                                    borderSide:
                                  BorderSide(color: Colors.pink, width: 4.0),
                                
                                
                                ),
                            contentPadding: EdgeInsets.only(
                                bottom: 10.0, left: 30.0, right: 0.0),
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(20.0),
                                  bottomRight: Radius.circular(20.0)),
                              borderSide:
                                  BorderSide(color: Colors.pink, width: 5.0),
                            ),
                          ),
                        ),
                      );
  }
}