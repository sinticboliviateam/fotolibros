import 'package:flutter/material.dart';

class ButtonRaizen extends StatelessWidget {
 
  const ButtonRaizen({Key key,  this.text:"", @required this.onPressed,}) : super(key: key);

  final GestureTapCallback onPressed;
  final String text;
  void _handlerOnclick(){
      onPressed();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
      height: 50.0,
      child: RaisedButton(
        onPressed: () {_handlerOnclick();},
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.teal,Colors.tealAccent],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.only(topRight:Radius.circular(30.0) ,bottomRight:Radius.circular(30.0) )),
          child: Container(
            constraints: BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
            alignment: Alignment.center,
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 18.0),
            ),
          ),
        ),
      ),
    ),
    );
  }
}
