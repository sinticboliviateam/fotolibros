import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Colors.pink
                /* image: DecorationImage(
              
                fit: BoxFit.fitHeight,
              ), */
                ),
            alignment: Alignment.center,
          ),
          Positioned(
              top: 30,
              left: 0.0,
              right: 0.0,
          
              child: Container(
               /*  width: 300,
                height: 200, */
                /* color: Colors.blueAccent, */
                child: Padding(padding: EdgeInsets.all(30.0),
                child: Center(child: Column(
                  children: <Widget>[
                    Icon(
                  Icons.library_books,size: 90.0,color: Colors.white,
                ),
                Text("Hola Amigo",style: new TextStyle(color: Colors.white, fontSize: 18),)
                  ],
                ),)
                ),
              )),
          Positioned(
              bottom: 0,
              left: 0.0,
              right: 0.0,
              top:0,
              child: Container(
               /*  width: 300,
                height: 200, */
                /* color: Colors.blueAccent, */
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/img/foto_family.jpg'),
                    fit: BoxFit.fitWidth,
                    alignment: Alignment.center
                  ),
                ),
              )),
          Positioned(
              bottom: 80,
              left: 0.0,
              right: 0.0,
              child: Container(
                width: 300,
                height: 200,
                /* color: Colors.blueAccent, */
                child: Card(
                  child: Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Center(
                      child: new Text(
                        "Lorem Ipsum es simplemente el texto de relleno de las imprentas y  el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un ",
                        style: new TextStyle(
                          fontSize: 16,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              )),
          Positioned(
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                  width: 500,
                  height: 90,
                  alignment: Alignment.bottomCenter,
                  color: Colors.transparent,
                  child: AppBar(
                    actions: <Widget>[
                      IconButton(
                        icon: new Icon(
                          Icons.arrow_forward,
                        ),
                        onPressed: () {},
                      )
                    ],
                    backgroundColor: Colors.transparent,
                    elevation: 0.0,
                  ))),
        ],
      ),
    );
  }
}
