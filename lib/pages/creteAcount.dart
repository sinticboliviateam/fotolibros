import 'package:flutter/material.dart';
import 'package:fotolibroapp/componets/inputTextField.dart';

class CreateAcount extends StatefulWidget {
  @override
  _CreateAcountState createState() => _CreateAcountState();
}

class _CreateAcountState extends State<CreateAcount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: new Text(""),
          actions: <Widget>[
            IconButton(
              icon: new Icon(Icons.check,color: Colors.pink,size: 30.0,),
              onPressed: () { Navigator.pushNamed(context, '/welcome');},
            )
          ],
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        body: SingleChildScrollView(child:Center(
            child: Stack(
            
          alignment: Alignment.center,
          children: <Widget>[
            Center(child: Container(
              child: Column(
                children: <Widget>[
                             ],
              ),
            )),
            
            Container(
                alignment: Alignment.center,
                /* color: Colors.blue, */
                height: 500.0,
                width: 400.0,
                padding: EdgeInsets.only( left: 50, right: 50),
                child: Center(
                    child: Column(
                  children: <Widget>[
                     Icon( Icons.supervised_user_circle,size: 130,color: Colors.teal,),
                     Padding(
                       padding: EdgeInsets.all(5.0),
                       child:Text("Crear tu cuenta",
                     style: new TextStyle(
                       fontSize: 30.0,
                       fontWeight: FontWeight.w300
                     ),
                     ), ),
                     
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: InputTextField(
                        placeholder: "Nombre de usuario",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: InputTextField(
                        placeholder: "Número de teléfono",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: InputTextField(
                        placeholder: "Correo electrónico",
                      ),
                      
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: InputTextField(
                        placeholder: "Contraseña",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: InputTextField(
                        placeholder: "Confirmar contraseña",
                      ),
                    )
                  ],
                )))
          ],
        )))); 
  }
}
