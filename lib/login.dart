import 'package:flutter/material.dart';
import 'package:fotolibroapp/componets/cardLogin.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
 return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/img/foto.jpg'),
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          Positioned(
             bottom: 0.0,
            left: 0.0,
            right: 0.0,
          
            child:CardLogin()
          ),
           Positioned(
             top: 0.0,
            left: 0.0,
            right: 0.0,
            child:Container(
              
              child: AppBar(
                title: new Center(
                  child: Text("Iniciar sessión", style: new TextStyle(
                    fontSize: 25.0,
                  ),
                )
                
                
                ),
              
              backgroundColor: Colors.transparent,
               elevation: 0.0,
               leading: IconButton(
                 icon: new Icon(Icons.arrow_back,size: 25.0,),
                 onPressed: (){},
               ) ,
               actions: <Widget>[
                IconButton(
                  icon: new Icon(Icons.arrow_forward,size: 25.0),
                  onPressed: (){
                    
                  },
                ) 
               ],
              ),
            )
          ),
        ],
      ),
    );
  }
}
